@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row"><br>
            <div class="col-md-2">
              <img class="card-img-top" style="border-radius:50%" 
              src="{{(!empty($adminData->profile_photo_path))?
                  url('upload/admin_images/'.$adminData->profile_photo_path):
                    url('upload/no_image.jpg')}}" heigth="100%" width="100%" alt=""><br>
            <ul class="list-grouplist-group-flush">
                <a href="" class="btn btn-primary btn-sm btn-block">Home</a>
                <a href="{{route('user.profile')}}" class="btn btn-primary btn-sm btn-block">Profile Update</a>
                <a href="" class="btn btn-primary btn-sm btn-block">Change Password</a>
                <a href="{{route('user.logout')}}" class="btn btn-danger btn-sm btn-block">Logout</a><br>
            </ul>
            
            </div>
            <div class="col-md-2">

            </div>
            <div class="col-md-6">
                <h3 class="text-center"><span class="text-danger">Hi...</span>
               <strong>{{Auth::user()->name}}</strong> Update Your Profile</h3>

           
                <div class="card-body">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label class="info-title" for="exampleInputEmail1">Name </label>
                            <input type="text" id="name" name="name" class="form-control unicase-form-control text-input" value="{{$user->name}}" >
                        </div>
                        <div class="form-group">
                            <label class="info-title" for="exampleInputEmail1">Email </label>
                            <input type="email" id="email" name="email" class="form-control unicase-form-control text-input" value="{{$user->email}}">
                        </div>
                        <div class="form-group">
                            <label class="info-title" for="exampleInputEmail1">Phone Number </label>
                            <input type="number" id="phone" name="phone" class="form-control unicase-form-control text-input" value="{{$user->phone}}" >
                        </div>
                        <div class="form-group">
                            <label class="info-title" for="exampleInputEmail1">Image</label>
                            <input type="file" id="profile_photo_path" name="profile_photo_path" class="form-control unicase-form-control text-input" >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger">Update</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>

</div>


@endsection