@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row"><br>
            <div class="col-md-2">
              <img class="card-img-top" style="border-radius:50%" 
              src="{{(!empty($adminData->profile_photo_path))?
                  url('upload/admin_images/'.$adminData->profile_photo_path):
                    url('upload/no_image.jpg')}}" heigth="100%" width="100%" alt="">
            <ul class="list-grouplist-group-flush">
                <a href="" class="btn btn-primary btn-sm btn-block">Home</a>
                <a href="{{route('user.profile')}}" class="btn btn-primary btn-sm btn-block">Profile Update</a>
                <a href="" class="btn btn-primary btn-sm btn-block">Change Password</a>
                <a href="{{route('user.logout')}}" class="btn btn-danger btn-sm btn-block">Logout</a><br>
            </ul>
            
            </div>
            <div class="col-md-2">

            </div>
            <div class="col-md-6">
                <h3 class="text-center"><span class="text-danger">Hi...</span>
            <strong>{{Auth::user()->name}}</strong> Welcome To Easy Online Shop }}</h3>

            </div>

        </div>

    </div>

</div>


@endsection